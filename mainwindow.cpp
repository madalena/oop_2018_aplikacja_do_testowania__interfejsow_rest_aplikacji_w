#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
#include <QStateMachine>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mNetManager = new QNetworkAccessManager();
    //mNetManager = new QNetworkRequest()

    auto stateMachine = new QStateMachine(this);

    auto startup = new QState(stateMachine);
    auto getData = new QState(stateMachine);
    auto viewData = new QState(stateMachine);

    startup->assignProperty(ui->pushButton, "enabled", true);
    startup->assignProperty(ui->textEdit, "readOnly", true);

    viewData->assignProperty(ui->pushButton, "enabled", true);
    viewData->assignProperty(ui->textEdit, "readOnly", true);

    startup->addTransition(ui->pushButton, SIGNAL(clicked(bool)), getData);

    connect(getData, SIGNAL(entered()), this, SLOT(getData()));

    getData->addTransition(this, SIGNAL(success()), viewData);

    viewData->addTransition(ui->pushButton, SIGNAL(clicked(bool)), getData);

    stateMachine->setInitialState(startup);

    stateMachine->start();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OnGetDataClicked(QNetworkReply* reply)
{
    if(!reply->error()) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

        QJsonObject root = doc.object();

        //ui->textEdit->append(root.keys().at(0) + ": " + root.value(root.keys().at(0)).toString());

        //QJsonValue jv = root.value("employees");

        //QJsonValue jv = root.value("values");

        //qDebug() << root["forecast"].toString();

        //QString json = root["current"].toString();
        //QJsonObject obj = jsonArray.at(0).toObject();
        //QJsonObject jv = root.value(QString("current"));
        //QJsonObject obj = jv.at(0).toObject();

       // QJsonValue subObj = obj["value"];
        //QJsonValue obj = root.value("current");


        QVariantMap root_map = root.toVariantMap();
        QVariantMap current_map = root_map["current"].toMap();
        ui->textEdit->append("From date time: " + current_map["fromDateTime"].toString());
        QVariantList values_list = current_map["values"].toList();
        for(int i = 0; i<values_list.count(); ++i) {
            QVariantMap objAt = values_list.at(i).toMap();
            ui->textEdit->append(objAt["name"].toString() + ": " + objAt["value"].toString());
        }

        /*QJsonValue jv = root.value("current");

        if(jv.isArray()) {
            QJsonArray ja = jv.toArray();

            for(int i = 0; i< ja.count(); i++) {
                QJsonObject subtree = ja.at(i).toObject();

                //ui->textEdit->append(subtree.value("firstName").toString() + " " + subtree.value("lastName").toString());
                ui->textEdit->append(subtree.value("name").toString() + " " + subtree.value("value").toString());

            }
        }*/

        //ui->textEdit->append(QString::number(root.value("number").toInt()));

    }

    reply->deleteLater();
}

void MainWindow::getData()
{
    ui->textEdit->clear();
    connect(mNetManager, &QNetworkAccessManager::finished, this, &MainWindow::OnGetDataClicked);
    //mNetManager->get(QNetworkRequest(QUrl("http://www.evileg.ru/it_example.json")));

    QNetworkRequest request=QNetworkRequest(QUrl("https://airapi.airly.eu/v2/measurements/installation?indexType=AIRLY_CAQI&installationId=204"));

    //request.setRawHeader("Accept", "application/json");
    request.setRawHeader("apikey", "b7zmQRBKYgiAsLKSowvVlwwOVZrwF1CM");

    mNetManager->get(request);

    //mNetManager->get(QNetworkRequest(QUrl("https://airapi.airly.eu/v2/measurements/installation?indexType=AIRLY_CAQI&installationId=204")));

    emit success();
}

