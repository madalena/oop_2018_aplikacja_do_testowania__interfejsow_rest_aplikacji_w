#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QtNetwork>

class QByteArray;
class QNetworkAccessManager;
class QNetworkReply;
class QPushButton;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //void BuildMainWindow(); //builds startup states

    //void GetData(); //gets data from website

    //void NetworkCleanup(); //cleaning network

private slots:
    void OnGetDataClicked(QNetworkReply *reply);
    void getData();

    //void OnDataReadyToRead();
    //void OnListReadFinished();
    //void OnStoryReadFinished();

signals:
    void success();

private:
    Ui::MainWindow *ui;

    int mInstallationId;

    QPushButton * mButtonRefresh;

    QWidget * mPanelStories;

    //QNetworkAccessManager * mNetManager;
    QNetworkAccessManager * mNetManager;
    QNetworkReply * mNetReply;
    QByteArray * mDataBuffer;
};

#endif // MAINWINDOW_H
